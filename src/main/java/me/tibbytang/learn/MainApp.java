package me.tibbytang.learn;

import io.objectbox.Box;
import io.objectbox.BoxStore;

public class MainApp {
    public static void main(String[] args) {
        BoxStore store = MyObjectBox.builder().name("objectbox-notes-db").build();
        Box<Student> box = store.boxFor(Student.class);
        Student student = new Student();
        student.setAge(25);
        student.setName("tibbytang");
        long id = box.put(student);
        System.out.println(id);
    }
}
